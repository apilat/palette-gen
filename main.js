const assert = function (condition, message) {
    if (!condition)
        throw Error('Assertion failed: ' + (message || '<No message>'));
};

Array.prototype.shuffle = function () {
    for (var i = this.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
    }
};

Number.prototype.clamp = function (min, max) {
    return Math.min(Math.max(this, min), max);
};

function randomUniform(min, max) {
    return Math.random() * (max - min) + min;
}

function randomNormal(sigma, mu) {
    // https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
    let z = Math.sqrt(-2 * Math.log(Math.random())) * Math.cos(2 * Math.PI * Math.random());
    return z * sigma + mu;
};

class Color {
    constructor(type, data, description) {
        if (type === "rgb") {
            assert(data.length === 3, `invalid rgb data ${data}`);
            this._rgb = data;
            this._rgb[0] = this._rgb[0].clamp(0, 1);
            this._rgb[1] = this._rgb[1].clamp(0, 1);
            this._rgb[2] = this._rgb[2].clamp(0, 1);
        } else if (type == "hsl") {
            assert(data.length === 3, `invalid hsl data ${data}`);
            this._hsl = data;
            this._hsl[0] = (this._hsl[0] % 360 + 360) % 360;
            this._hsl[1] = this._hsl[1].clamp(0, 1);
            this._hsl[2] = this._hsl[2].clamp(0, 1);
        } else {
            assert(false, `invalid color type ${type}`);
        }

        this.description = description || 'No description';
    }

    get rgb() {
        if (this._rgb)
            return this._rgb.slice();

        // https://en.wikipedia.org/wiki/HSL_and_HSV
        const [h, s, l] = this.hsl;
        const f = n => {
            const a = s * Math.min(l, 1 - l);
            const k = (n + h / 30 + 12) % 12;
            return l - a * Math.max(-1, Math.min(k - 3, 9 - k, 1));
        };
        const [r, g, b] = [f(0), f(8), f(4)];
        this._rgb = [r, g, b];
        return this._rgb.slice();
    }
    get r() {
        return this.rgb[0];
    }
    get g() {
        return this.rgb[1];
    }
    get b() {
        return this.rgb[2];
    }
    get hex() {
        const conv = x => Math.floor(x * 255).toString(16).padStart(2, '0');
        return '#' + conv(this.r) + conv(this.g) + conv(this.b);
    }

    get hsl() {
        if (this._hsl)
            return this._hsl.slice();

        // https://en.wikipedia.org/wiki/HSL_and_HSV
        const [r, g, b] = this.rgb;
        const v = Math.max(r, g, b);
        const min = Math.min(r, g, b);
        const c = v - min;
        const l = (v + min) / 2;
        let h;
        if (c === 0) {
            h = 0;
        } else if (v === r) {
            h = 60 * (0 + (g - b) / c);
        } else if (v === g) {
            h = 60 * (2 + (b - r) / c);
        } else if (v === b) {
            h = 60 * (4 + (r - g) / c);
        }
        const s = (v - l) / Math.min(l, 1 - l);
        this._hsl = [h, s, l];
        return this._hsl.slice();
    }
    get h() {
        return this.hsl[0];
    }
    get s() {
        return this.hsl[1];
    }
    get l() {
        return this.hsl[2];
    }

    get y() {
        // https://stackoverflow.com/a/596241/5304124
        return 0.299 * this.r + 0.587 * this.g + 0.114 * this.b;
    }

    equals(other) {
        return Math.abs(this.r - other.r) < 1.0e-7
            && Math.abs(this.g - other.g) < 1.0e-7
            && Math.abs(this.b - other.b) < 1.0e-7;
    }
}

class ColorDisplay extends HTMLElement {
    constructor(color) {
        super();

        this.attachShadow({mode: 'open'});

        const textColor = color.y > 0.588 ? 'black' : 'white';
        const style = document.createElement('style');
        // TODO Export most of this to a proper stylesheet
        style.textContent = `
        :host {
            color: ${textColor};
            background-color: rgb(${color.r * 255},${color.g * 255},${color.b * 255});
            display: flex;
            flex-direction: column;
            text-align: center;
        }
        `;

        const name = document.createElement('h2');
        name.textContent = color.hex;

        const fmtText = document.createElement('span');
        fmtText.textContent = color.description;

        this.shadowRoot.append(style, name, fmtText);
    }
}
customElements.define('color-display', ColorDisplay);

class ColorGenerator {
    constructor(inputCount, generatedCount, generator) {
        this.inputCount = inputCount;
        this.generatedCount = generatedCount;
        this.generator = generator;
    }

    generate(inputs) {
        assert(inputs.length === this.inputCount, `incorrect input count: expected ${this.inputCount}, got ${inputs}`);
        const outputs = (this.generator)(inputs);
        assert(outputs.length === this.generatedCount, `incorrect output count: expected ${this.generatedCount}, got ${outputs}`);
        return outputs;
    }
}

function regenerateColors() {
    const count = parseInt(document.getElementById('color_count').count.value);
    const bitsRemoved = 8 - parseInt(document.getElementById('bit_count').bits.value);
    const onlyRandom = document.getElementById('random').checked;

    let colors = [];
    const generators = [
        new ColorGenerator(0, 1,
            _ => [new Color("rgb", [randomUniform(0, 1), randomUniform(0, 1), randomUniform(0, 1)], 'Randomly generated')],
        ),
        new ColorGenerator(1, 1,
            inputs => {
                let hsl = inputs[0].hsl;
                hsl[0] += 180;
                return [new Color("hsl", hsl, `Complementary color of ${inputs[0].hex}`)];
            },
        ),
        new ColorGenerator(1, 1,
            inputs => {
                let hsl = inputs[0].hsl;
                let shift = (Math.random() > 0.5 ? 1 : -1) * randomNormal(0.1, 0.3).clamp(0, 0.5);
                hsl[2] += shift;
                return [new Color("hsl", hsl, `Luminance shift of ${(shift * 100).toFixed(0)}% from ${inputs[0].hex}`)];
            },
        ),
        new ColorGenerator(1, 2, inputs => {
            let hsl = inputs[0].hsl;
            let shift = randomNormal(25, 75).clamp(0, 180);
            return [new Color("hsl", [hsl[0] + shift, hsl[1], hsl[2]], `Hue shift of ${shift.toFixed(0)}\u00b0 right from ${inputs[0].hex}`),
            new Color("hsl", [hsl[0] - shift, hsl[1], hsl[2]], `Hue shift of ${shift.toFixed(0)}\u00b0 left from ${inputs[0].hex}`)];
        }),
    ];

    while (colors.length < count) {
        let gen;
        if (onlyRandom) {
            gen = generators[0];
        } else {
            gen = generators[Math.floor(Math.random() * generators.length)];
        }

        if (colors.length >= gen.inputCount && count - colors.length >= gen.generatedCount) {
            colors.shuffle();
            const inputs = colors.slice(0, gen.inputCount)
            for (const color of gen.generate(inputs)) {
                if (colors.every(c => !c.equals(color))) {
                    colors.push(color);
                }
            }
        }
    }

    const limitBits = x => ((255 * x) >> bitsRemoved << bitsRemoved) / 255;
    const limitBitsInColor = c => new Color("rgb", c.rgb.map(limitBits), c.description);
    const content = document.getElementById('content');
    content.replaceChildren(...colors.map(c => new ColorDisplay(limitBitsInColor(c))));
}

regenerateColors();
document.addEventListener('keypress', evt => {
    if (evt.key == ' ') {
        regenerateColors();
    }
});
document.getElementById('generate').addEventListener('click', regenerateColors);
document.getElementById('color_count').addEventListener('input', regenerateColors);
document.getElementById('bit_count').addEventListener('input', regenerateColors);
document.getElementById('full_random').addEventListener('input', regenerateColors);
